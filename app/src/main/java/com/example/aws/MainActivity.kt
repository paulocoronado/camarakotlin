package com.example.camera

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.example.aws.R
import java.io.ByteArrayOutputStream
import java.io.File


object Constants {

    val ACCESS_ID:String = "AKIAW67NOATYAMYDRZN5"
    val SECRET_KEY:String = "la9r2stNPu6ga6HcZdKu+fcElzuOzrX+MaOf4S2K"
    val BUCKET_NAME: String = "pccphotobucket"

}

class MainActivity : AppCompatActivity() {
    var ivPhoto: ImageView? = null
    var bTakePicture: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ivPhoto = findViewById(R.id.imageView)
        bTakePicture = findViewById(R.id.uploadButton)

        //If you want to use the camera, you need to request permission from the user.
        val permisos = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )

        //Request permission from the user.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permisos, 1)
        }
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            takePicture()
        } else {
            //If the user does not grant permission, the app will close.
            Toast.makeText(this, "No se puede acceder a la cámara", Toast.LENGTH_SHORT).show()
        }
    }

    private fun takePicture() {
        val ejecutarResultado = registerForActivityResult<Intent, ActivityResult>(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == RESULT_OK) {
                val extras = result.data!!.extras
                val imageBitmap = extras!!["data"] as Bitmap?
                ivPhoto!!.setImageBitmap(imageBitmap)


                //Convert Bitmap ti java.io.File
                val bytes = ByteArrayOutputStream()
                imageBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                val f = File(
                    externalCacheDir!!.absoluteFile.toString() + File.separator + "image.jpg"
                )
                f.createNewFile()
                val fo = f.outputStream()
                fo.write(bytes.toByteArray())
                fo.close()

                val upload = uploadImage(f!!, this)
            }
        }
        ejecutarResultado.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
    }

    private fun uploadImage(image:File, context: MainActivity) {
        var creds: BasicAWSCredentials = BasicAWSCredentials(Constants.ACCESS_ID, Constants.SECRET_KEY)
        var s3Client: AmazonS3Client = AmazonS3Client(creds, Region.getRegion(Regions.US_EAST_2))

        //Upload the image to S3
        TransferNetworkLossHandler.getInstance(context)
        val trans = TransferUtility.builder().context(applicationContext).s3Client(s3Client).build()
        //val observer: TransferObserver = trans.upload(Constants.BUCKET_NAME,filePath!!.lastPathSegment, file)//manual storage permission
        val observer: TransferObserver = trans.upload(
            Constants.BUCKET_NAME,
            Constants.SECRET_KEY,
            image
        )



        observer.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (state == TransferState.COMPLETED) {
                    Log.d("msg","success")
                } else if (state == TransferState.FAILED) {
                    Log.d("msg","fail")
                }
            }
            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {

                if(bytesCurrent == bytesTotal){
                    ivPhoto!!.setImageResource(R.drawable.ic_launcher_foreground)
                }
            }
            override fun onError(id: Int, ex: Exception) {
                Log.d("error",ex.toString())
            }
        })


    }



    }
